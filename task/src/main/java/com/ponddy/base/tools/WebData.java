package com.ponddy.base.tools;

/**
 * Created by lixin on 2016/11/2.
 */
public class WebData {
    //系统配置
    public static final Long SS_TIMEOUT = 360000L * 1000 * 4;   //session过期时间(毫秒)
    public static final String SS_KEY = "SSKEY";   //cookie中session对应的key
}