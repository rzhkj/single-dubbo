/*
 *  Copyright (c) 2017. 郑州仁中和科技有限公司.保留所有权利.
 *                        http://www.rzhkj.com/
 *       郑州仁中和科技有限公司保留所有代码著作权.如有任何疑问请访问官方网站与我们联系.
 *       代码只针对特定客户使用，不得在未经允许或授权的情况下对外传播扩散.恶意传播者，法律后果自行承担.
 *       本代码仅用于龐帝業務系统.目.
 */

package com.ponddy.core.enums;

/**
 * 上传文件资料类型
 * Created by lixin on 2017/6/2.
 */
public enum UploadTypeEnum {
    BASIC("基础资料"),
    CONTRACT("合同资料"),
    ILLNESSCASE("病例资料"),
    INSPECT("体检资料"),
    NURSING("护理资料"),
    TYPELESS("其它资料");
    public String type;

    UploadTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static UploadTypeEnum getEnum(String type) {
        for (UploadTypeEnum uploadTypeEnum : UploadTypeEnum.values()) {
            if (uploadTypeEnum.name().toLowerCase().equals(type.toLowerCase())) {
                return uploadTypeEnum;
            }
        }
        return null;
    }


}
